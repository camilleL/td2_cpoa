package exercice2;
/**
 * @author logeart1u
 * Classe AdresseIP
 */
public class AdresseIP {
	
	//ATTRIBUT 
	String ip;
	
	/**
	 * Constructeur
	 * @param s
	 */
	public AdresseIP(String s){
		this.ip = s;
	}
	
	/**
	 * M�thode toString
	 */
	public String toString(){
		return(ip);
	}
	
}
