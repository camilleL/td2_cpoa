package exercice2;
import java.util.*;
import java.io.*;
/**
 * @author LOGEART THOUVENIN
 * Classe ListeIp
 * Ensemble des adresses IP des machines ayant effectu�es
 * des op�ations sur le serveur
 */
public class ListeIp {

	//ATTRIBUTS
	
	/** Contient les IPs des machines*/
	private Set<String> ips;
	
	//CONSTRUCTEURS
	
	/**
	 * Constructeur
	 * Construit une liste d'ip
	 */
	public ListeIp(){
		this.ips = new HashSet<String>();
	}
	
	//METHODES
	
	/**
	 * M�thode chargerFichier
	 * Permet de lire le fichier
	 * et de remplir l'ensemble
	 * @param name	nom du fichier
	 */
	public void chargerFichier(String name)throws Exception{
		Reader r = new FileReader(new File(name));
		BufferedReader bf = new BufferedReader(r);
		String ligne = bf.readLine();
		while(ligne != null){
			String[] res = ligne.split(" ");
			this.ips.add(res[0]);
			ligne = bf.readLine();
		}
		bf.close();
	}
	
	/**
	 * M�thode toString
	 * Permet d'aficher l'ensemble des adresses IP
	 * @return String
	 */
	public String toString(){
		String rep = "";
		for(String s : this.ips){
			rep += s + "\n";
		}
		return rep;
	}
	
	/**
	 * M�thode principale
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception{
		ListeIp l = new ListeIp();
		l.chargerFichier("logs.txt");
	}
	
}
