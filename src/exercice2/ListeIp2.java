package exercice2;
import java.util.*;
import java.io.*;
/**
 * @author LOGEART THOUVENIN
 * Classe ListeIp
 * Ensemble des adresses IP des machines ayant effectu�es
 * des op�ations sur le serveur
 */
public class ListeIp2 {

	//ATTRIBUTS
	
	/** Contient les IPs des machines*/
	private Set<AdresseIP> ips;
	
	//CONSTRUCTEURS
	
	/**
	 * Constructeur
	 * Construit une liste d'ip
	 */
	public ListeIp2(boolean b){
		if(b)
			this.ips = new TreeSet<AdresseIP>();
		else
			this.ips = new HashSet<AdresseIP>();
	}
	
	//METHODES
	
	/**
	 * M�thode chargerFichier
	 * Permet de lire le fichier
	 * et de remplir l'ensemble
	 * @param name	nom du fichier
	 */
	public void chargerFichier(String name)throws Exception{
		Reader r = new FileReader(new File(name));
		BufferedReader bf = new BufferedReader(r);
		String ligne = bf.readLine();
		while(ligne != null){
			String[] res = ligne.split(" ");
			//this.ips.add(res[0]);
			ligne = bf.readLine();
		}
		bf.close();
	}
	
	/**
	 * M�thode toString
	 * Permet d'aficher l'ensemble des adresses IP
	 * @return String
	 */
	public String toString(){
		String rep = "";
		for(AdresseIP s : this.ips){
			rep += s + "\n";
		}
		return rep;
	}
	
	/**
	 * M�thode principale
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception{
		ListeIp2 l = new ListeIp2(false);
		l.chargerFichier("logs.txt");
		System.out.println(l);
	}
	
}
