package test;

import static org.junit.Assert.*;

import org.junit.*;

import td2_cpoa.Formation;

import java.util.*;

/**
 * @author LOGEART THOUVENIN
 * Classe de test pour la classe Formation
 * 
 */

public class TestFormation {
	
	Formation f;
	
	@Before
	public void initialise(){
		f = new Formation("021356");
		f.getMatieres().put("maths",1.5);
		f.getMatieres().put("fran�ais",2.0);
	}
	
	/**
	 * test d'ajout de mati�re qui n'existe pas encore dans la formation
	 */
	@Test
	public void testAddMatInexistante(){
		assertTrue(f.addMat("histoire",1.5));
	}
	
	/**
	 * test d'ajout d'une mati�re deja existante dans la formation
	 */
	@Test
	public void testAddMatExistante(){
		assertFalse(f.addMat("maths",1.5));
	}
	
	/**
	 * test de suppression d'une mati�re existante dans la formation
	 */
	@Test
	public void testSupMatExistante(){
		assertTrue(f.suppMat("maths"));
	}
	
	/**
	 * test de suppression d'une mati�re inexistante dans la formation
	 */
	@Test
	public void testSupMatInexistante(){
		assertFalse(f.suppMat("histoire"));
	}
	
	
}
