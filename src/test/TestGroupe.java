package test;
import static org.junit.Assert.*;

import org.junit.*;

import td2_cpoa.*;

/**
 * @author LOGEART THOUVENIN
 * Classe de test pour la classe Groupe
 */
public class TestGroupe {

	Groupe g1;
	Formation form1, form2;
	Identit� id1, id2;
	Etudiant et1, et2;
	
	@Before
	public void initialise(){
		form1 = new Formation("formation1");		
		g1 = new Groupe(form1);
		id1 = new Identit�("123","Dupont","Jean");
		et1 = new Etudiant(id1,form1);
		
		form2 = new Formation("formation2");
		id2 = new Identit�("145","Dupond","Paul");
		et2 = new Etudiant(id2,form2);
	}
	
	@Test
	public void test_ajouterEtudiant() {
		boolean res = g1.addEts(et1);
		assertTrue(res);
	}
	
	@Test
	public void test_ajouterEtudiantDejaExistant(){
		g1.addEts(et1);
		boolean res = g1.addEts(et1);
		assertFalse(res);
	}
	
	@Test
	public void test_ajouterEtudiantMauvaiseFormation(){
		boolean res = g1.addEts(et2);
		assertFalse(res);
	}
	
	@Test
	public void test_supprimerEtudiant(){
		g1.addEts(et1);
		boolean res = g1.supEts(et1);
		assertTrue(res);
	}
	
	@Test
	public void test_suppEtudiantInexistant(){
		g1.addEts(et1);
		boolean res = g1.supEts(et2);
		assertFalse(res);
	}
	
	@Test
	public void test_calculerMoyGrpMat(){
		
	}
	
	@Test
	public void test_calculerMoyGeneraleGrp(){
		
	}

}
