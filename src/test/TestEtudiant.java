package test;
import static org.junit.Assert.*;

import org.junit.*;

import java.util.*;

import td2_cpoa.*;
/**
 * @author LOGEART THOUVENIN
 * Classe de test pour la classe Etudiant
 */
public class TestEtudiant {

	Formation form;
	Identit� id;
	Etudiant et1;
	
	@Before
	public void initialise(){
		form = new Formation("id1");
		form.addMat("maths", 1);
		id = new Identit�("id2","Dupont","Jean");
		et1 = new Etudiant(id,form);
	}
	
	@Test
	public void test_AjouterNoteMatExiste(){
		et1.ajouterNote(10, "maths");
	    double resultat = et1.getResultats().get("maths").get(0);
		assertEquals("La note de 10 est ajout� en maths", 10.0, resultat,0.01);
	}
	
	@Test
	public void test_AjouterNoteMatInexistante(){
		boolean res = et1.ajouterNote(10, "algo");
		assertFalse(res);
		
	}
	
	@Test
	public void test_AjouterNoteMauvaisIntervalle(){
		boolean res = et1.ajouterNote(21,"maths");
		assertFalse(res);
	}
	
	@Test
	public void test_CalculerMoyMat(){
		et1.ajouterNote(10, "maths");
		et1.ajouterNote(20,"maths");
		double res = et1.calculerMoyMat("maths");
		assertEquals("La moyenne en maths est de 15", 15.0, res, 0.01);
	}
	
	@Test
	public void test_CalculerMoyGenerale(){
		et1.ajouterNote(10, "maths");
		et1.ajouterNote(20,"maths");
		form.addMat("algo", 1);
		et1.ajouterNote(10, "algo");
		et1.ajouterNote(20, "algo");
		double res = et1.calculerMoyGen();
		assertEquals("La moyenne g�n�rale est de 15", 15.0, res, 0.01);
		
	}
	
}
