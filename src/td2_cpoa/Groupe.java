package td2_cpoa;

import java.util.Set;
import java.util.TreeSet;

public class Groupe {

	/**
	 * Attributs de la classe Groupe
	 *  formGroupe : formation suivit par le groupe
	 *  groupeEts : etudiants faisant partis du groupe
	 */
	private Formation formGroupe;
	private TreeSet<Etudiant> groupeEts;
	
	/**
	 * Constructeur de la classe Groupe
	 * @param f
	 */
	public Groupe (Formation f){
		formGroupe = f;
		groupeEts = new TreeSet<Etudiant>();
	}
	
	/**
	 * M�thode addEts, ajoute un �tudiant au groupe s'il n'est pas d�ja pr�sent dedans
	 * @param e : �tudiant a ajouter
	 * @return res : vaut true si l'ajout a �t� effectu�
	 */
	public boolean addEts(Etudiant e){
		boolean key = false;
		boolean res = false;
		if(e.getFormation()==formGroupe){
			key = groupeEts.contains(e);
			if(!key){
				groupeEts.add(e);
				res =true;
			}
		}	
		return res;
	}
	
	/**
	 * M�thode supEts , supprime un �tudiant du groupe s'il est bien pr�sent dedans
	 * @param e : �tudiant a supprim�
	 * @return res , vaut true si la suppresion a bien �t� effectu�e
	 */
	public boolean supEts(Etudiant e){
		boolean key = false;
		boolean res = false;
		if(e.getFormation()==formGroupe){
			key=groupeEts.contains(e);
			if(key){
				groupeEts.remove(e);
				res=true;
			}
		}	
		return res;
	}
	
	/**
	 * M�thode calculMoyMatGrp , calcul la moyenne d'un groupe pour une mati�re donn�e
	 * @param matiere : mati�re dont on cherche la moyenne du groupe
	 * @return moyenne , moyenne du groupe dans la mati�re pass�e en param�tre de la m�thode
	 */
	public double calculMoyMatGrp(String matiere){
		double somme=0,moyenne;
		int div = 0;
		for(Etudiant e : groupeEts){
			somme = somme + e.calculerMoyMat(matiere);
			div++;
		}
		
		moyenne=somme/div;
		return moyenne;
	}
	/**
	 * M�thode calculMoyGenGrp , calcul la moyenne generale du groupe
	 * @return moyGenGrp , moyenne generale du groupe
	 */
	public double calculMoyGenGrp(){
		double moyGenGrp,somme=0;
		int div = 0;
		for(Etudiant e : groupeEts){
			somme = somme + e.calculerMoyGen();
			div++;
		}
		moyGenGrp=somme/div;
		return moyGenGrp;
	}
}
