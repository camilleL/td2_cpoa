package td2_cpoa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * 
 * @author LOGEART THOUVENIN
 * Classe Etudiant , repr�sente un �tudiant
 */
public class Etudiant {
/**
 * Attributs de la classe Etudiant
 * id :  identidiant de l'�tudiant
 * res : resultats de l'�tudiant	
 * formation : formation suivit par l'�tudiant
 */
	private Identit� id;
	private HashMap<String,List<Double>> resultats;
	private Formation formation;
	
	/**
	 * Constructeur de la classe Etudiant
	 * @param ident
	 * @param form
	 */
	public Etudiant(Identit� ident,Formation form){
		this.id=ident;
		this.formation=form;
		this.resultats= new HashMap<String,List<Double>>();
		Set<String> mat = formation.getMatieres().keySet();
		for(String s : mat){
			resultats.put(s,new ArrayList<Double>());
		}
	}
	
	public Identit� getId() {
		return id;
	}

	public void setId(Identit� id) {
		this.id = id;
	}

	public HashMap<String, List<Double>> getResultats() {
		return resultats;
	}

	public void setResultats(HashMap<String, List<Double>> resultats) {
		this.resultats = resultats;
	}

	public Formation getFormation() {
		return formation;
	}

	public void setFormation(Formation formation) {
		this.formation = formation;
	}
	
	/**
	 * M�thode ajouterNote, ajoute une note � l'�tudiant dans une mati�re donn�
	 * @param note
	 * @param matiere
	 */
	public boolean ajouterNote(double note,String matiere){
		boolean key=this.resultats.containsKey(matiere);
		boolean res = false;
		if(key){
			if (note>=0 && note<=20){
				this.resultats.get(matiere).add(note);
				res=true;
			}
		}	
		return res;
	}
	
	/**
	 * M�thode calculerMoyMat, calcule la moyenne pour une mati�re donn�e
	 * @param matiere
	 * @return
	 */
	public double calculerMoyMat(String matiere){
		double moyenne;
		double somme = 0;
		int div =0;
			List res= resultats.get(matiere);
			Iterator <Double> i = res.iterator();
			while(i.hasNext()){
				somme= somme+i.next();
				div++;
			}
		moyenne = somme/div;
		return moyenne;
	}

	/**
	 * M�thode calculerMoyGen, calcule la moyenne g�nerale de l'�tudiant
	 * @return
	 */
	public double calculerMoyGen(){
		double moyGenerale,somme = 0,coef;
		Set<String> format = formation.getMatieres().keySet();
		Iterator<String> i = format.iterator();
		int div=0;
		while(i.hasNext()){
			coef =+ formation.getMatieres().get(i);
			somme =+ (calculerMoyMat(i.toString())*coef);
			div++;
		}
		moyGenerale=somme/div;
		return moyGenerale;
	}
}
