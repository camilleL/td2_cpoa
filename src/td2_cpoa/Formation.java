package td2_cpoa;
import java.util.*;
/**
 * @author LOGEART THOUVENIN
 * Classe Formation
 * Formation suivie par un eleve
 */
public class Formation {

	//ATTRIBUTS

	/** Identifiant de la formation*/
	private String idFormat;

	/** Collection de matieres avec leurs coefficients*/
	private HashMap<String,Double> matiereFormat;


	//CONSTRUCTEURS
	
	/**
	 * Constructeur � 2 param�tres
	 * @param	String
	 * @param	HashMap<String,Double>
	 */
	public Formation(String nId){
		
		this.idFormat = nId;
		this.matiereFormat = new HashMap<String,Double>();
	}


	//METHODES

	/**
	 * Methode addMat
	 * Permet d'ajouter une matiere dans la formation
	 * ajout impossible si mati�re d�j� pr�sente
	 * @param String	mati�re � ajouter
	 * @param double	coefficient de la mati�re
	 * @return boolean	true seulement si ajout possible
	 */
	public boolean addMat(String nMat, double coef){
		boolean key = this.matiereFormat.containsKey(nMat);
		//on v�rifie que la mati�re n'est pas d�j� pr�sente
		boolean res = false;
		if(!key){
			this.matiereFormat.put(nMat, coef);
			res = true;
		}
		return res;
	}

	/**
	 * Methode suppMat
	 * Permet de supprimer une mati�re dans la formation
	 * @param String	mati�re � supprim�
	 * @return boolean	true seulement si on peut supprimer
	 */
	public boolean suppMat(String mat){
		boolean key = this.matiereFormat.containsKey(mat);
		//on v�rifie que la mati�re est pr�sente
		boolean res = false;
		if(key){
			this.matiereFormat.remove(mat);
			res = true;
		}
		return res;
	}
	
	/**
	 * M�thode get
	 * Permet de r�cup�rer le coefficient d'une mati�re
	 * @param mat	mati�re
	 * @return double	coefficient
	 */
	public double getCoefficient(String mat){
		double coef;
		boolean key = this.matiereFormat.containsKey(mat);
		//on v�rifie que la mati�re est pr�sente
		if(key)
			coef = this.matiereFormat.get(mat);
		else
			coef = 1;	//matiere inexistante donc coef 1
		
		return coef;
	}

	//Getter & Setter

	/**
	 * Getter de idFormat
	 * @return String
	 */
	public String getIdFormat() {
		return idFormat;
	}

	/**
	 * Setter de idFormat
	 * @param idFormat
	 */
	public void setIdFormat(String idFormat) {
		this.idFormat = idFormat;
	}

	/**
	 * Getter de matiereFormat
	 * @return	HashMap<String,Double>
	 */
	public HashMap<String, Double> getMatieres() {
		return matiereFormat;
	}

	/**
	 * Setter de matiereFormat
	 * @param mat
	 */
	public void setMatieres(HashMap<String, Double> mat) {
		this.matiereFormat = mat;
	}

}
