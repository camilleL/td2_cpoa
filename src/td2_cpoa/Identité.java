package td2_cpoa;
/**
 * 
 * @author LOGEART THOUVENIN
 * Classe Identit�
 * D�finie l'identit� d'un �l�ve
 */
public class Identit� {

	/**
	 * Attributs de la classe Identit�
	 * nip : num�ro d'identification personnel de l'�tudiant
	 * nom : nom de l'�tudiant
	 * prenom : pr�nom de l'�tudiant
	 */
	private String nip;
	private String nom;
	private String prenom;

	/**
	 * Constructeur de la classe Identit�
	 * @param id
	 * @param name
	 * @param prnom
	 */
	public Identit� (String id,String name,String prnom){
		this.nip=id;
		this.nom=name;
		this.prenom=prnom;
	}

	/**
	 * M�thode getNip ,renvoie l'identifiant de l'�tudiant
	 * @return nip
	 */
	public String getNip() {
		return nip;
	}

	/**
	 * M�thode getNom, renvoie le nom de l'�tudiant
	 * @return nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * M�thode getPrenom, renvoie le pr�nom de l'�tudiant
	 * @return prenom
	 */
	public String getPrenom() {
		return prenom;
	}


}
